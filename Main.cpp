#include <iostream>
#include <SDL2/SDL.h>
#include "Bezier.h"
#include "Circle.h"
#include "FillColor.h"
#include "Ellipse.h"
using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
    //First we need to start up SDL, and make sure it went ok
    if (SDL_Init(SDL_INIT_VIDEO) != 0){
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
    //Make sure creating our window went ok
    if (win == NULL){
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    //Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
    if (ren == NULL){
        SDL_DestroyWindow(win);
        std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
    SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE
    
    SDL_SetRenderDrawColor(ren, 20, 100, 0, 255);

    Bresenham_Line(float(200), float(200), float(400), float(300), ren);

     Vector2D v1(100,500),v2(500,500),v3(200,200),vTL(50,250), vBR(300,120);
     SDL_Color fillColor,fillColor1,fillColor2,red,boundaryColor;
     fillColor = { 20, 0, 100, 255};
     fillColor1 = {100,20,0,255};
     fillColor2 = {0, 200, 200, 255};
     red = {255,0,0,255};
     boundaryColor = {0,255,0,255};
      TriangleFill( v1, v2, v3,ren,fillColor);
      CircleFill(100, 200 , 100, ren, fillColor1);
      RectangleFill(vTL,vBR,ren,fillColor2);
      FillIntersectionRectangleCircle(vTL, vBR, 100, 200 , 100, ren, red);
    

    // Uint32 pixel_format = SDL_GetWindowPixelFormat(win);
    // BoundaryFill4(win, v1 , pixel_format, ren, fillColor, boundaryColor);

    SDL_Color blue;
	blue = { 0, 87, 249, 255 };
	SDL_SetRenderDrawColor(ren, 0, 87, 249, 255);

	Bresenham_Line(10, 10, 50, 10,ren);
	Bresenham_Line(10, 10, 10, 60, ren);
	Bresenham_Line(50, 60, 10, 60, ren);
	Bresenham_Line(50, 60, 50, 10, ren);

	Vector2D v4(30,35);
	Uint32 pixel_fomat = SDL_GetWindowPixelFormat(win);
	// BoundaryFill4(win, v4, pixel_fomat, ren, red, blue);


	// Vector2D A(10,10), B(10,30), C(25,25), start(15,25);
	// Bresenham_Line(A.x, A.y, B.x, B.y, ren);
	// Bresenham_Line(B.x, B.y, C.x, C.y, ren);
	// Bresenham_Line(A.x, A.y, C.x, C.y, ren);
	// BoundaryFill4(win, start, pixel_fomat, ren, red, blue);

    //two circle intersec
//    int xc1 = 200, yc1 = 200, R1 = 100, xc2 = 250, yc2 = 250, R2 = 70;
//    CircleFill(xc1, yc1 , R1, ren, fillColor1);
//    CircleFill(xc2, yc2 , R2, ren, fillColor2);
//    FillIntersectionTwoCircles(xc1,  yc1,  R1,  xc2,  yc2, R2, ren, fillColor);
//    
    //circle and elip
    // int a = 100, b =50;
    // BresenhamDrawEllipse(xc1, yc1, a, b, ren);
    // CircleFill(xc1, yc1 , R2, ren, fillColor2);
    // FillIntersectionEllipseCircle(xc1 ,yc1, a, b, xc1, yc1, R2, ren, red);


    // DrawCurve3(ren, v1, v2, vTL,vBR);

    SDL_RenderPresent(ren);

   
    //Take a quick break after all that hard work
    //Quit if happen QUIT event
    SDL_RenderPresent;
    bool running = true;


    Vector2D pB[4];
    pB[0] = {100,100}; pB[1] = {300,200};  pB[2] = {400,100}; pB[3] = {300,100}; // Bezier point;

    SDL_Rect butt[4]; //button;
    butt[0] = {int(pB[0].x),int(pB[0].y),15,15};
    butt[1] = {int(pB[1].x),int(pB[1].y),15,15};
    butt[2] = {int(pB[2].x),int(pB[2].y),15,15};
    butt[3] = {int(pB[3].x),int(pB[3].y),15,15};

    bool state = false; int numbutt;
    while(running)
    {
        
        //If there's events to handle
        if(SDL_PollEvent( &event))
        {
            int x,y;
            SDL_GetMouseState(&x, &y);
            if(event.button.button == SDL_BUTTON_LEFT && event.type == SDL_MOUSEBUTTONDOWN )
            {
                for(int k =0; k<= 3; k++)
                    if((x-butt[k].x)*(x-butt[k].x-butt[k].w) <= 0 && (y-butt[k].y)*(y-butt[k].y - butt[k].h) <= 0)
                    {
                        state = true;
                        numbutt = k;
                    }
            }
            if(event.button.button == SDL_BUTTON_LEFT && event.type == SDL_MOUSEBUTTONUP)
                state = false;
            if(state)
            {
                pB[numbutt].x = x;
                pB[numbutt].y = y;
                butt[numbutt].x = x;
                butt[numbutt].y = y;
            }
            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                running = false;
            }
        }
        //clear screen
        SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
        SDL_RenderClear(ren);

        SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
        for(int k =0; k<=3; k++)
            BresenhamDrawCircle(pB[k].x,pB[k].y,10, ren);

        SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.b, fillColor.g, fillColor.a);
        for(int k =0; k<3; k++)
            Bresenham_Line(pB[k].x, pB[k].y, pB[k+1].x, pB[k+1].y, ren);
        
        //draw Bezier
        SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
        DrawCurve3(ren, pB[0],pB[1],pB[2],pB[3]);

        SDL_RenderPresent(ren);
    }


    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    SDL_Quit();

    return 0;
}
