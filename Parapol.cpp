#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
    int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int dp =  A - 1, p = 0, q = 0;

	while(p <= A)
	{
		Draw2Points(xc, yc, p ,q, ren);
		if(dp<0)
		{
			dp += 2*A - (2*p+3);
			q++;
		}
		else
		{
			dp += - (2*p+3);
		}
		p ++;

	}	

	int w, h;
    
    SDL_GetRendererOutputSize(ren, &w, &h);

    dp = 1 - 2*A;
    while(q <= h)
    {
    	Draw2Points(xc, yc, p ,q, ren);
    	if(dp < 0)
    	{
    		dp += 4*p + 4 - 4*A;
    		p++;
    	}
    	else
    		dp += -4*A;
    	q++;
    }
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int dp =  - A + 1, p = 0, q = 0;

	while(p <= A)
	{
		Draw2Points(xc, yc, p ,q, ren);
		if(dp>=0)
		{
			dp += - 2*A + (2*p+3);
			q--;
		}
		else
		{
			dp += + (2*p+3);
		}
		p ++;

	}	

	int w, h;
    
    SDL_GetRendererOutputSize(ren, &w, &h);

    dp = -(1 - 2*A);
    while(q >= -h)
    {
    	Draw2Points(xc, yc, p ,q, ren);
    	if(dp >= 0)
    	{
    		dp -= 4*p + 4 - 4*A;
    		p++;
    	}
    	else
    		dp += 4*A;
    	q--;
    }
}
