#include "Clipping.h"

using namespace std;



RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;

    if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
 
    if (c1 != 0 && c2 != 0 && c1&c2 != 0)

        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
    CODE c1 = Encode(r,P1), c2 = Encode(r,P2);
    int checkcase = CheckCase(c1,c2);

    int kkk = 0;
    while(checkcase==3)
    {
        
        cout << c1 << "   " << c2 << kkk << endl ;
 
    while(checkcase==3)
    {

        ClippingCohenSutherland(r,P1,P2);
        c1 = Encode(r,P1);
        c2 = Encode(r,P2);
        checkcase = CheckCase(c1,c2);
 
        kkk ++ ;
    }
    if(checkcase==2)
       return 0;
 
    Q2 = P2;
    Q1 = P1;

    return 1;


    }
    if(checkcase==2)
        return 0;
 
        Q2 = P2;
        Q1 = P1;
        return 1;
    
    
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
    CODE c1 = Encode(r,P1), c2 = Encode(r,P2);
    int m = (P2.y-P1.y)/(P2.x - P1.x);
    if(c1&LEFT!=0)
    {
        P1.y = P1.y + m*(r.Left-P1.x);
        P1.x = r.Left;
    }
    if(c1&RIGHT!=0)
    {
        P1.y = P1.y + m*(r.Right-P1.x);
        P1.x = r.Right;
    }
    if(c1&TOP!=0)
    {
        P1.x = P1.x + (r.Top-P1.y)/m;
        P1.y = r.Top;
    }    
    if(c1&BOTTOM!=0)
    {
        P1.x = P1.x + (r.Bottom-P1.y)/m;
        P1.y = r.Bottom;
    }

    if(c2&LEFT!=0)
    {
        P2.y = P2.y + m*(r.Left-P2.x);
        P2.x = r.Left;
    }
    if(c2&RIGHT!=0)
    {
        P2.y = P2.y + m*(r.Right-P2.x);
        P2.x = r.Right;
    }
    if(c2&TOP!=0)
    {
        P2.x = P2.x + (r.Top-P2.y)/m;
        P2.y = r.Top;
    }    
    if(c2&BOTTOM!=0)
    {
        P2.x = P2.x + (r.Bottom-P2.y)/m;
        P2.y = r.Bottom;
    }    
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
    float tmin=0, tmax=1;

    int p,q;


    for(int i=0; i< 4; i++)
        {
            if(i==0) { p = -(P2.x - P1.x); q = -(r.Left - P1.x);}
            if(i==1) { p = (P2.x - P1.x); q = r.Right-P1.x;}
            if(i==2) { p = -(P2.y - P1.y); q = -(r.Bottom - P1.y);}
            if(i==3) { p = (P2.y - P1.y); q = r.Top - P1.y;}

           
            SolveNonLinearEquation(p,q,tmin,tmax);

        

        }


    Q1.x = P1.x + (P2.x-P1.x)*tmin;
    Q1.y = P1.y + (P2.y - P1.y)* tmin;
    Q2.x = P1.x + (P2.x-P1.x)*tmax;
    Q2.y = P1.y + (P2.y - P1.y)* tmax;
    return 1;

}
