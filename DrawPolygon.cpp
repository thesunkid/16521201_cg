#include "DrawPolygon.h"
#include <iostream>
using namespace std;
void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = M_PI/2;
	for(int i =0; i<3 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		phi += M_PI*2/3;
	}
	for(int i =0;i<3;i++)
		Bresenham_Line(x[i],y[i],x[(i+1)%3],y[(i+1)%3],ren);
 

	// Bresenham_Line(x[0],y[0],x[2],y[2],ren);
 
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4],y[4];
	float phi = M_PI/4;
 
	for(int i =0; i<4 ;i++)
 
	for(int i =0; i<3 ;i++)
 
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		phi += M_PI/2;
	}
	for(int i =0;i<4;i++)
		Bresenham_Line(x[i],y[i],x[(i+1)%4],y[(i+1)%4],ren);
   
 
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = M_PI/2;
	for(int i =0; i<5 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		phi += M_PI*2/5;
	}
	for(int i =0;i<5;i++)
		Bresenham_Line(x[i],y[i],x[(i+1)%5],y[(i+1)%5],ren);	
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6],y[6];
	float phi = 0;
	for(int i =0; i<6 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		phi += M_PI/3;
	}
	for(int i =0;i<6;i++)
		Bresenham_Line(x[i],y[i],x[(i+1)%6],y[(i+1)%6],ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = M_PI/2;
	for(int i =0; i<5 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		phi += M_PI*2/5;
	}
	for(int i =0;i<5;i++)
		Bresenham_Line(x[i],y[i],x[(i+2)%5],y[(i+2)%5],ren);		
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
 
	int x[5], y[5], xr[5], yr[5];
	float phi = M_PI/2;
	float r = R*sin(M_PI/10)/sin(7*M_PI/10);
	for(int i =0; i<5 ;i++)
	{
		x[i] = xc + round(R*cos(phi));
		y[i] = yc - round(R*sin(phi));
		xr[i] = xc - round(r*cos(phi));
		yr[i] = yc + round(r*sin(phi));
		phi += M_PI*2/5;
		//cout << "Diem so " << i << " co x-xr: "  << x[i] << " " << xr[i] << " va y-yr: " << y[i] << " " << yr[i] << endl;
 
	
	float phi = M_PI/2;
	float r = R*sin(M_PI / 10)/sin(7 * M_PI / 10);
	for(int i =0; i<5 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		xr[i] = xc - int(r*cos(phi) + 0.5);
		yr[i] = yc + int(r*sin(phi)+ 0.5);
		phi += M_PI*2/5;
 
	}int x[5], y[5],xr[5],yr[5];
	for(int i =0;i<5;i++)
	{
		Bresenham_Line(x[i],y[i],xr[(i+3)%5],yr[(i+3)%5],ren);
		Bresenham_Line(xr[(i+3)%5],yr[(i+3)%5],x[(i+1)%5],y[(i+1)%5],ren);
	}
 
}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8],xr[8],yr[8];
	float phi = M_PI/2, rphi = M_PI*5/8;
 
	float r = R*sin(M_PI/8)/sin(6*M_PI/8);

	for(int i =0; i<8 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		xr[i] = xc - int(r*cos(rphi) + 0.5);
		yr[i] = yc + int(r*sin(rphi)+ 0.5);
		phi += M_PI/4;
		rphi += M_PI/4;
	}
	for(int i =0;i<8;i++)
    {	xr[i] = xc - int(r*cos(rphi) + 0.5);
		yr[i] = yc + int(r*sin(rphi)+ 0.5);
		phi += M_PI/4;
		rphi += M_PI/4;
	}
	for(int i =0;i<8;i++)
	{
		Bresenham_Line(x[i],y[i],xr[(i+4)%8],yr[(i+4)%8],ren);
		Bresenham_Line(x[(i+1)%8],y[(i+1)%8],xr[(i+4)%8],yr[(i+4)%8],ren);
	}	
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5], xr[5], yr[5];
	float phi = startAngle;
	float r = R*sin(M_PI/10)/sin(7*M_PI/10);
	for(int i =0; i<5 ;i++)
	{
		x[i] = xc + round(R*cos(phi));
		y[i] = yc - round(R*sin(phi));
		xr[i] = xc - round(r*cos(phi));
		yr[i] = yc + round(r*sin(phi));
		phi += M_PI*2/5;
		//cout << "Diem so " << i << " co x-xr: "  << x[i] << " " << xr[i] << " va y-yr: " << y[i] << " " << yr[i] << endl;
	int x[5], y[5];
	int xr[5], yr[5];
	float phi = startAngle;
	float r = sin(M_PI / 10)*R/sin(7 * M_PI / 10);
	float rphi = phi + M_PI/5;
	for(int i =0; i<5 ;i++)
	{
		x[i] = xc + int(R*cos(phi)+ 0.5);
		y[i] = yc - int(R*sin(phi)+ 0.5);
		xr[i] = xc - int(r*cos(phi) + 0.5);
		yr[i] = yc + int(r*sin(phi)+ 0.5);
		phi += M_PI*2/5;
	}
	for(int i =0;i<5;i++)
	{
		Bresenham_Line(x[i],y[i],xr[(i+3)%5],yr[(i+3)%5],ren);
		Bresenham_Line(xr[(i+3)%5],yr[(i+3)%5],x[(i+1)%5],y[(i+1)%5],ren);
	}

}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float phi = M_PI/2;
	while (r>0)
	{
		DrawStarAngle(xc, yc,r,phi,ren);
 
		r = r*sin(M_PI/10)/sin(7*M_PI/10);
 
		r = sin(M_PI / 10)*r / sin(7 * M_PI / 10);
 
		phi += M_PI;
	}
}
