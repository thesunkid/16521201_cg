#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    //7 points
    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc + x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc - x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc + y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc - y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc + y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);  
    
    new_x = xc - y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);  
    
                 
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int dp = 3 - 2*R, p = 0, q = R;
	
	while(p<=q)
	{
		Draw8Points(xc, yc, p, q, ren);
		p ++;
		if(dp<0)
			dp += 4*p + 6;
		else
		{
			dp += 4*(p-q) + 10;
			q --;
		}
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int dp = 0, p = 0, q = R;

	while(p <= q)
	{
		Draw8Points(xc, yc, p, q, ren);
		if(dp <= 0)
		{
			p++;
			dp += 2*p + 1;

		}
		else
		{
			q --;
			dp -= 2*q +1;
		}
	}
}
