#include "Ellipse.h"
#define ROUND(a) (int)(a+0.5)
#include <math.h>

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);


}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    int dp = 2*b*b - 2*b*a*a + a*a;
    int p = 0, q = b;
    int temp = ROUND(a*a/(sqrt(a*a + b*b)));

    while(p <= temp)
    {
    	Draw4Points(xc, yc, p, q,ren);
    	
    	if(dp < 0)
    		dp += b*b*(4*p + 6);
    	else
    	{
    		dp += b*b*(4*p + 6) + 4*a*a - 4*q*a*a;
    		q --;
    	}
    	p++;
    }

    dp = 2*a*a - 2*a*b*b + b*b;
    p = a, q = 0;

    while(p > temp)
    {
    	Draw4Points(xc, yc, p, q,ren);

    	if(dp < 0)
    		dp += a*a*(4*q + 6);
    	else
    	{
    		dp += a*a*(4*q + 6) + 4*b*b - 4*p*b*b;
    		p --;
    	}
    	q++;
    }

 
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    int a2 = a*a, b2 = b*b;
    float dp = b2 - a2*b + a2/4, p = 0, q = b;
    int temp = ROUND(a2/sqrt(a2+b2));

    while(p <= temp)
    {
        Draw4Points(xc, yc, p, q,ren);

        if(dp < 0)
            dp +=  b2*(2*p + 3);
        else
        {
            dp+= b2*(2*p + 3) + 2*a2 - 2*a2*q;
            q--;
        }
        p ++;
    }

    dp = a2 - b2*a + b2/4, p = a, q = 0;

    while (p > temp)
    {
        Draw4Points(xc, yc, p, q,ren);

        if(dp < 0)
            dp += a2*(2*q + 3);
        else
        {
            dp += a2*(2*q + 3) + 2*b2 - 2*p*b2;
            p --;
        }
        q++;
    
    }

}